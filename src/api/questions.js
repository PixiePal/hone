import axios from 'axios'

import { API_ROOT } from './api-config'

export {getInitialQuestions, getPublicTest, getPrivateTest}

function getInitialQuestions () {
  return axios.get(`${API_ROOT}/questions`)
    .then(response => {
      console.log('Fetched questions: ', response.data)
      return response.data
    })
    .catch(error => {
      console.error(error)
    })
}

function getPublicTest () {
  return axios.get(`${API_ROOT}/api/public`)
    .then(response => {
      console.log('Fetched public api: ', response.data)
      return response.data
    })
    .catch(error => {
      console.error(error)
    })
}

function getPrivateTest () {
  return axios.get(`${API_ROOT}/api/private`)
    .then(response => {
      console.log('Fetched private api: ', response.data)
      return response.data
    })
    .catch(error => {
      console.error(error)
    })
}
