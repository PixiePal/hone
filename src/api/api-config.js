let backendHost

const hostname = window && window.location && window.location.hostname

if (hostname === 'english-hone.herokuapp.com') {
  backendHost = 'http://hone-api.herokuapp.com'
} else {
  backendHost = 'http://localhost:3001'
}

export const API_ROOT = `${backendHost}`
