import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

class LogoutButton extends Component {

  static propTypes = {
    auth: PropTypes.object.isRequired
  };

  logout = () => {
    this.props.auth.logout()
  }

  render() {
    return (
      <Button onClick={this.logout}>LOGOUT</Button>
    );
  }

}
export default LogoutButton;
