import React, {Component} from 'react'
import PropTypes from 'prop-types'

import LoadSign from '../load-sign/LoadSign'

class AuthCallback extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired
  }

  render () {
    const { auth, location, history } = this.props

    if (/access_token|id_token|error/.test(location.hash)) {
      auth.handleAuthenticationFinishedEvent(history)
    }

    return (
      <LoadSign {...this.props} />
    )
  }
}

export default AuthCallback
