export const HOST_URL = 'http://localhost:3000'

export const HOME_PATH = '/home'

export const AUTH0_CALLBACK_PATH = '/callback'

export const AUTH0_CONFIG = {
  domain: 'english-hone.eu.auth0.com',
  clientId: 'FCfgrD86qIFOlKO4bb0xx99jaqn544xS',
  callbackUrl: HOST_URL + AUTH0_CALLBACK_PATH // after authentication, users shall be redirected to this page
}
