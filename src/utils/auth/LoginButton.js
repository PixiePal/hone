import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'reactstrap';

class LoginButton extends Component {

  static propTypes = {
    auth: PropTypes.object.isRequired
  };

  login = () => {
    this.props.auth.login()
  }

  render() {
    return (
      <Button onClick={this.login}>LOG IN</Button>
    );
  }

}
export default LoginButton;
