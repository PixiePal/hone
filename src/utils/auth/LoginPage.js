import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Redirect} from 'react-router-dom'
import LoginButton from './LoginButton'


export default class LoginPage extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  };

  state = {
    redirectToReferrer: false
  };

  render() {
    const { from } = this.props.location.state || { from: { pathname: "/" } }
    const { redirectToReferrer } = this.state

    if (redirectToReferrer) {
      return <Redirect to={from} />
    }

    return (
      <p>Please  <LoginButton auth={this.props.auth} /> to view this page!
      </p>
    )
  }
}
