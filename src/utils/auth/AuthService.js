import auth0 from 'auth0-js'

import history from '../history'
import { AUTH0_CONFIG, HOME_PATH } from './variables'

export default class AuthService {
  auth0 = new auth0.WebAuth({
    domain: AUTH0_CONFIG.domain,
    clientID: AUTH0_CONFIG.clientId,
    redirectUri: AUTH0_CONFIG.callbackUrl, // after authentication, users shall be redirected to this page
    audience: `https://${AUTH0_CONFIG.domain}/userinfo`, // Open ID Connect Conformant Authentication
    responseType: 'token id_token', // give me an access_token and an id_token
    scope: 'openid profile' // get the Open ID Connect Conformant Authentication and the user profile information
  })

  login = () => {
    this.auth0.authorize()
  }

  /*  Looks for the result of authentication in the URL hash.
  Then, the result is processed with the parseHash method from auth0.js */
  handleAuthenticationFinishedEvent = () => {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult)
        console.log('Authentication result: ' + authResult)
      } else if (err) {
        history.replace(HOME_PATH)
        console.log(err)
      }
    })
  }

  /* Sets the user's access token, ID token, and the access token's expiry time */
  setSession = (authResult) => {
    // Set the time that the access token will expire at
    let expiresAt = JSON.stringify(
      (authResult.expiresIn * 1000) + new Date().getTime())
    localStorage.setItem('access_token', authResult.accessToken)
    localStorage.setItem('id_token', authResult.idToken)
    localStorage.setItem('expires_at', expiresAt)
    // navigate to the home route
    history.replace(HOME_PATH)
  }

  getAccessToken = () => {
    const accessToken = localStorage.getItem('access_token')
    if (!accessToken) {
      throw new Error('No access token found')
    }
    return accessToken
  }

  getProfile = (callback) => {
    let accessToken = this.getAccessToken()
    this.auth0.client.userInfo(accessToken, (err, profile) => {
      if (profile) {
        this.userProfile = profile
      }
      callback(err, profile)
    })
  }

  /* Removes the user's tokens and expiry time from browser storage */
  logout = () => {
    // Clear access token and ID token from local storage
    localStorage.removeItem('access_token')
    localStorage.removeItem('id_token')
    localStorage.removeItem('expires_at')
    this.userProfile = null
    // navigate to the home route
    history.replace(HOME_PATH)
  }

  /* Checks whether the expiry time for the user's access token has passed */
  isAuthenticated = () => {
    let expiresAt = JSON.parse(localStorage.getItem('expires_at'))
    return new Date().getTime() < expiresAt
  }
}
