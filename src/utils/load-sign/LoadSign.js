import React from 'react'
import loading from './loading.svg'

const LoadSign = () => (
  <div className='loadSign'>
    <img src={loading} alt='loading' />
  </div>
)

export default LoadSign
