import React, {Component, Fragment} from 'react'
import { Route, Router } from 'react-router-dom'

import Home from './components/home/Home'
import Profile from './components/home/Profile'
import AuthService from './utils/auth/AuthService'
import AuthCallback from './utils/auth/AuthCallback'
import LoginPage from './utils/auth/LoginPage'
import PrivateRoute from './utils/auth/PrivateRoute'
import {AUTH0_CALLBACK_PATH} from './utils/auth/variables'
import history from './utils/history'

import './index.css'

class App extends Component {
  authService = new AuthService();

  render () {
    return (
      <Router history={history}>
        <Fragment>
          <Route path='/' render={(props) =>
            <Home auth={this.authService} {...props} />} />
          <Route path='/login' render={(props) =>
            <LoginPage auth={this.authService} {...props} />} />
          <PrivateRoute path='/profile' auth={this.authService} component={Profile} />
          <Route path={AUTH0_CALLBACK_PATH} render={(props) =>
            <AuthCallback auth={this.authService} {...props} />
          } />
        </Fragment>
      </Router>
    )
  }
}

export default App
