import React from 'react'

const QuizButton = ({text, isWrong, clickCallback,
  doDisappear, doMove, moveDescriptor, moveParity}) => {
  let statusOverlay = ''
  if (isWrong) {
    statusOverlay = <img src={require('./x.svg')} className='wrong' alt='X'
      onAnimationEnd={(event) => event.stopPropagation()} />
  }

  let cssClass = ''
  if (doDisappear) {
    cssClass += 'disappear'
  }
  if (doMove) {
    cssClass += 'move' + (moveDescriptor < 0 ? 'Up' : 'Down') +
    '-by-' + Math.abs(moveDescriptor) + '-' + moveParity
  }

  return (
    <li key={text}
      className={cssClass}>
      <button className={isWrong ? 'wrong' : 'init'}
        onClick={() => { clickCallback(text) }}
        onAnimationEnd={(event) => event.stopPropagation()}
        onTransitionEnd={(event) => event.stopPropagation()}>
        {text}
      </button>
      {statusOverlay}
    </li>
  )
}

export default QuizButton
