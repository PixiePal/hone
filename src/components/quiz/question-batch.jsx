import React, {Component} from 'react'
import PropTypes from 'prop-types'

import Question from './question'

export default class QuestionBatch extends Component {
  constructor (props) {
    super(props)

    this.unsolvedQuestions = props.questions

    let currentIndex = this._randomizeInt(this.unsolvedQuestions.length)

    this.state = {currentIndex: currentIndex}
  }

  render () {
    let quiz = this.unsolvedQuestions[this.state.currentIndex]

    return (
      <section className='quiz'>
        <Question questionId={quiz.id}
          start={quiz.start} end={quiz.end}
          options={quiz.options} correct={quiz.correct}
          nextPleaseCallBack={this._selectNextQuestion}
        />
      </section>
    )
  }

  _randomizeInt (max) {
    return Math.floor(Math.random() * max)
  }

  _selectNextQuestion = () => {
    let newQuizzes = this.unsolvedQuestions.slice()
    newQuizzes.splice(this.state.currentIndex, 1)

    if (newQuizzes.length === 0) {
      console.log('Done')
      return
    }

    this.unsolvedQuestions = newQuizzes

    const newIndex = this._randomizeInt(newQuizzes.length)

    this.setState({currentIndex: newIndex})
  }
}

QuestionBatch.propTypes = {
  questions: PropTypes.array.isRequired
}
