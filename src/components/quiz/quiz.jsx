import React, {Component} from 'react'

import {Container, Row, Col} from 'reactstrap'

import { getInitialQuestions } from '../../api/questions'
import LoadSign from '../../utils/load-sign/LoadSign'
import './quiz.css'
import QuestionBatch from './question-batch'

export default class Quiz extends Component {
  state = { questions: null }

  getQuestions () {
    getInitialQuestions().then((questions) => {
      this.setState({ questions })
    })
  }

  componentDidMount () {
    this.getQuestions()
  }

  render () {
    return (
      <section className="quizWrapper">
        <Container >
          <Row>
            <Col xs="12" sm={{ size: 8, offset: 2 }}>
              {this.state.questions ? (
                <QuestionBatch questions={this.state.questions} />
              ) : (<LoadSign/>)}
            </Col>
          </Row>
        </Container>
      </section>
    )
  }
}
