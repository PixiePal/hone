import React, {Component} from 'react'
import PropTypes from 'prop-types'
import QuizButton from './quiz-button'

export default class Question extends Component {
  constructor (props) {
    super(props)

    this.state = {
      wrongList: [],
      isNew: true,
      isSolved: false
    }

    this.guess = this._guess.bind(this)
    this.animationCount = 0

    this._initMoveMap(props.options.length)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.questionId !== this.props.questionId) {
      this._initMoveMap(nextProps.options.length)

      this.setState({
        wrongList: [],
        isNew: true,
        isSolved: false
      })
    }
  }

  render () {
    let buttons = []
    let i = 0
    for (var key of this.props.options) {
      buttons.push(
        <QuizButton key={key} text={key}
          isWrong={this.state.wrongList.indexOf(key) >= 0}
          clickCallback={this.guess}
          doDisappear={this.state.isSolved && (key !== this.props.correct)}
          doMove={this.state.isSolved &&
            (key === this.props.correct) &&
            (this.moveMap[i] !== 0)}
          moveDescriptor={this.moveMap[i]}
          moveParity={this.moveMap.length % 2 !== 0 ? 'odd' : 'even'}
        />
      )
      i++
    }

    return (
      <div className={'question' +
        (this.state.isNew ? ' new' : '') +
        (this.state.isSolved ? ' solved' : '')}
        onAnimationEnd={({animationName}) => {
          if (animationName === 'solutionHighlight') {
            this.props.nextPleaseCallBack()
          }
        }}>
        <h2 className='quiz-start'>{this.props.start}</h2>
        <ul className='quiz-buttons'>
          {buttons}
        </ul>
        <h2 className='quiz-end'>{this.props.end}</h2>
      </div>
    )
  }

  _initMoveMap (optionCount) {
    this.moveMap = []
    let maxMove = Math.floor(optionCount / 2)
    for (let i = maxMove; i > 0; i--) {
      this.moveMap.push(i)
    }
    if (optionCount % 2 === 1) {
      this.moveMap.push(0)
    }
    for (let i = 1; i <= maxMove; i++) {
      this.moveMap.push(-i)
    }
  }

  _guess (option) {
    if (option === this.props.correct) {
      this.setState({
        isSolved: true,
        isNew: false
      })
    } else {
      this.setState((prevState) => (
        {
          wrongList: [...prevState.wrongList, option],
          isNew: false
        }))
    }
  }
}

Question.propTypes = {
  questionId: PropTypes.number.isRequired,
  start: PropTypes.string.isRequired,
  end: PropTypes.string.isRequired,
  options: PropTypes.array.isRequired,
  correct: PropTypes.string.isRequired,
  nextPleaseCallBack: PropTypes.func.isRequired
}
