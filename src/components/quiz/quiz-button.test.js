import React from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'

import {shallow} from 'enzyme'

import QuizButton from './quiz-button'

it('should render without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<QuizButton text='einein'
    isWrong={false} clickCallback={() => { console.log('clickCallback') }}
    doDisappear={false}
    doMove={false} moveDescriptor={0} moveParity='odd' />, div)
})

it('should render an initial button', () => {
  const button = shallow(<QuizButton text='das Mädchen'
    isWrong={false} clickCallback={() => { console.log('clickCallback') }}
    doDisappear={false}
    doMove={false} moveDescriptor={0} moveParity='odd' />)

  expect(button.find('button').length).toEqual(1)
  expect(button.find('button').text()).toEqual('das Mädchen')

  expect(button.find('button.wrong').length).toEqual(0)
  expect(button.find('button.init').length).toEqual(1)
  expect(button.find('button + img').length).toEqual(0)
  expect(button.find('li.disappear').length).toEqual(0)
})

it('should render an wrong button', () => {
  const button = shallow(<QuizButton text='das'
    isWrong clickCallback={() => { console.log('clickCallback') }}
    doDisappear={false}
    doMove={false} moveDescriptor={0} moveParity='odd' />)

  expect(button.find('button.wrong').length).toEqual(1)
  expect(button.find('button.init').length).toEqual(0)
  expect(button.find('button + img').length).toEqual(1)
})

it('should render disappering good button', () => {
  const button = shallow(<QuizButton text='das'
    isWrong={false}
    clickCallback={() => { console.log('clickCallback') }}
    doDisappear
    doMove={false} moveDescriptor={0} moveParity='odd' />)

  expect(button.find('li.disappear').length).toEqual(1)
})

it('should render a sliding direction UP', () => {
  const button = shallow(<QuizButton text='das'
    isWrong={false}
    clickCallback={() => { console.log('clickCallback') }}
    doDisappear={false}
    doMove moveDescriptor={-1} moveParity='odd' />)

  expect(button.find('li.disappear').length).toEqual(0)
  expect(button.find('li').is('.moveUp-by-1-odd')).toBeTruthy()
})

it('should render a sliding direction DOWN', () => {
  const button = shallow(<QuizButton text='das'
    isWrong={false}
    clickCallback={() => { console.log('clickCallback') }}
    doDisappear={false}
    doMove moveDescriptor={2} moveParity='odd' />)

  expect(button.find('li.disappear').length).toEqual(0)
  expect(button.find('li').is('.moveDown-by-2-odd')).toBeTruthy()
})

it('should render a sliding down button for ODD options', () => {
  const button = shallow(<QuizButton text='das'
    isWrong={false}
    clickCallback={() => { console.log('clickCallback') }}
    doDisappear={false}
    doMove moveDescriptor={1} moveParity='odd' />)

  expect(button.find('li').is('.moveDown-by-1-odd')).toBeTruthy()
})

it('should render a sliding down button for EVEN options', () => {
  const button = shallow(<QuizButton text='das'
    isWrong={false}
    clickCallback={() => { console.log('clickCallback') }}
    doDisappear={false}
    doMove moveDescriptor={2} moveParity='even' />)

  expect(button.find('li').is('.moveDown-by-2-even')).toBeTruthy()
})
