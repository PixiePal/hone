import React from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'

import {mount} from 'enzyme'

import QuestionBatch from './question-batch'
import Question from './question'

import {DEFAULT_QUESTIONS} from './default-questions.js'

it('should render Quiz without crashing', () => {
  const div = document.createElement('div')
  ReactDOM.render(<QuestionBatch questions={DEFAULT_QUESTIONS} />, div)
})

it('should initialize the questions', () => {
  let game = new QuestionBatch({
    questions: [
      { start: '',
        end: 'Apfel',
        options: ['der', 'die', 'das'],
        correct: 'der',
        id: 1
      },
      {
        start: 'Ich möchte',
        end: 'Apfel essen.',
        options: ['einen', 'eine', 'ein'],
        correct: 'einen',
        id: 2
      },
      {
        start: 'Die Tankstelle liegt',
        end: 'Kilometer von hier.',
        options: ['ein', 'eine', 'einen'],
        correct: 'einen',
        id: 3
      }
    ]
  })
  expect(game.unsolvedQuestions.length).toEqual(3)
})

it('should initialize one quiz', () => {
  let game = new QuestionBatch({
    questions: [
      {
        start: 'Ich möchte',
        end: 'Apfel essen.',
        options: ['einen', 'eine', 'ein'],
        correct: 'einen',
        id: 1
      }
    ]})
  expect(game.unsolvedQuestions.length).toEqual(1)
  expect(game.state.currentIndex).toEqual(0)
})

it('should select one quiz out of two', () => {
  let game = new QuestionBatch({
    questions: [
      { start: '',
        end: 'Apfel',
        options: ['der', 'die', 'das'],
        correct: 'der',
        id: 1
      },
      {
        start: 'Ich möchte',
        end: 'Apfel essen.',
        options: ['einen', 'eine', 'ein'],
        correct: 'einen',
        id: 2
      }
    ]
  })
  let firstQuizId = game.state.currentIndex
  expect(firstQuizId).toBeGreaterThanOrEqual(0)
  expect(firstQuizId).toBeLessThanOrEqual(1)
})

it('should render a question section', () => {
  const questions = [
    {
      start: 'Ich möchte',
      end: 'Apfel essen.',
      options: ['einen', 'eine', 'ein'],
      correct: 'einen',
      id: 1
    }
  ]
  const game = mount(<QuestionBatch questions={questions} />)

  expect(game.find('section').length).toEqual(1)
})

it('should initialize Question', () => {
  const questions = [{
    start: 'Ich möchte',
    end: 'Apfel essen.',
    options: ['einen', 'eine', 'ein'],
    correct: 'einen',
    id: 1}]

  let quiz = ReactTestUtils.renderIntoDocument(
    <QuestionBatch questions={questions} />)
  var question = ReactTestUtils.findRenderedComponentWithType(quiz, Question)

  expect(question.props.questionId).toBe(1)
  expect(question.props.start).toBe('Ich möchte')
  expect(question.props.end).toBe('Apfel essen.')
  expect(question.props.options.length).toBe(3)
  expect(question.props.correct).toBe('einen')
})

it('should render next quiz after Question animation ends', () => {
  const questions = [
    { start: '',
      end: 'Apfel',
      options: ['der', 'die', 'das'],
      correct: 'der',
      id: 1
    },
    {
      start: 'Ich möchte',
      end: 'Apfel essen.',
      options: ['einen', 'eine', 'das'],
      correct: 'einen',
      id: 2
    }
  ]

  const game = mount(<QuestionBatch questions={questions} />)
  const endBeforeClick = game.find('.quiz-end').text()

  // game.find('ul').simulate('click');
  game.find('.question').simulate('animationEnd',
    {animationName: 'solutionHighlight'})

  const endAfterClick = game.find('.quiz-end').text()

  if (endBeforeClick === 'Apfel essen.') {
    expect(endAfterClick).toEqual('Apfel')
    expect(game.find('button').at(0).text()).toEqual('der')
    expect(game.find('button').at(1).text()).toEqual('die')
    expect(game.find('button').at(2).text()).toEqual('das')
  } else {
    expect(endAfterClick).toEqual('Apfel essen.')
    expect(game.find('button').at(0).text()).toEqual('einen')
    expect(game.find('button').at(1).text()).toEqual('eine')
    expect(game.find('button').at(2).text()).toEqual('das')
  }

  expect(game.find('button').at(0).hasClass('wrong')).toBeFalsy()
  expect(game.find('button').at(0).hasClass('init')).toBeTruthy()

  expect(game.find('button').at(1).hasClass('wrong')).toBeFalsy()
  expect(game.find('button').at(1).hasClass('init')).toBeTruthy()

  expect(game.find('button').at(2).hasClass('wrong')).toBeFalsy()
  expect(game.find('button').at(2).hasClass('init')).toBeTruthy()
})
