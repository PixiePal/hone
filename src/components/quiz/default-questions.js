const DEFAULT_QUESTIONS = [
  { start: '',
    end: 'Apfel',
    options: ['der', 'die', 'das'],
    correct: 'der',
    id: 1
  },
  {
    start: 'Ich möchte',
    end: 'Apfel essen.',
    options: ['einen', 'eine', 'ein'],
    correct: 'einen',
    id: 2
  },
  {
    start: 'Die Tankstelle liegt',
    end: 'Kilometer von hier.',
    options: ['ein', 'eine', 'einen'],
    correct: 'einen',
    id: 3
  },
  {
    start: 'Die Bluse',
    end: 'war rosa.',
    options: ['der Mädchens', 'des Mädchen', 'des Mädchens'],
    correct: 'des Mädchens',
    id: 4
  },
  {
    start: 'Der Hund beißt',
    end: 'Mann.',
    options: ['der', 'dem', 'den'],
    correct: 'den',
    id: 5
  }
]

export {DEFAULT_QUESTIONS}
