import React from 'react'
import ReactDOM from 'react-dom'
import ReactTestUtils from 'react-dom/test-utils'

import {mount} from 'enzyme'

import Question from './question'
import QuizButton from './quiz-button'

it('should initialize Question class properly', () => {
  const question = ReactTestUtils.renderIntoDocument(<Question
    questionId={1}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['einen', 'eine', 'ein']} correct={'einen'}
    resolveCallBack={() => { console.log('resolveCallBack') }}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
    isSolved={false} />)

  expect(question.props.questionId).toBe(1)
  expect(question.props.start).toBe('Ich möchte')
  expect(question.props.end).toBe('Apfel essen.')
  expect(question.props.options.length).toBe(3)
  expect(question.props.correct).toBe('einen')
  expect(question.props.isSolved).toBe(false)
})

it('should render a Question with options', () => {
  const question = mount(<Question questionId={4}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['einen', 'eine', 'ein']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(question.find('.quiz-start').text()).toEqual('Ich möchte')
  expect(question.find('.quiz-end').text()).toEqual('Apfel essen.')
  expect(question.find('button').length).toEqual(3)
  expect(question.find('button').at(0).text()).toEqual('einen')
  expect(question.find('button').at(1).text()).toEqual('eine')
  expect(question.find('button').at(2).text()).toEqual('ein')

  expect(question.find('button.wrong').length).toEqual(0)
  expect(question.find('button.init').length).toEqual(3)
  expect(question.find('button + img').length).toEqual(0)
  expect(question.find('li.disappear').length).toEqual(0)
  expect(question.find('li.moveUp').length).toEqual(0)
  expect(question.find('li.moveDown').length).toEqual(0)
})

it('should render X for wrong click', () => {
  const question = mount(<Question questionId={2}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['einen', 'eine', 'ein']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  const endBeforeClick = question.find('.quiz-end').text()
  question.find('button').last().simulate('click')
  const endAfterClick = question.find('.quiz-end').text()

  expect(endAfterClick).toEqual(endBeforeClick)
  expect(question.find('button + img.wrong').length).toEqual(1)
  expect(question.find('button').at(2).hasClass('wrong')).toBeTruthy()
  expect(question.find('button').at(2).hasClass('init')).toBeFalsy()

  expect(question.find('button').at(0).hasClass('wrong')).toBeFalsy()
  expect(question.find('button').at(0).hasClass('init')).toBeTruthy()

  expect(question.find('button').at(1).hasClass('wrong')).toBeFalsy()
  expect(question.find('button').at(1).hasClass('init')).toBeTruthy()
})

it('should fade out wrong options for good click', () => {
  const question = mount(<Question questionId={6}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['einen', 'eine', 'ein']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  question.find('button').last().simulate('click')
  question.find('button').first().simulate('click')

  expect(question.find('button').at(2).hasClass('wrong')).toBeTruthy()
  expect(question.find('li').at(2).hasClass('disappear')).toBeTruthy()

  expect(question.find('button').at(1).hasClass('wrong')).toBeFalsy()
  expect(question.find('li').at(1).hasClass('disappear')).toBeTruthy()
})

it('should set parity to EVEN for 2 options', () => {
  const questionEven = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'einen']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(questionEven.find('QuizButton').get(0).props.moveParity).toEqual(
    'even')
  expect(questionEven.find('QuizButton').get(1).props.moveParity).toEqual(
    'even')
})

it('should set moveDescriptor to (1, -1) for 2 options', () => {
  const question = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'einen']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(question.find('QuizButton').get(0).props.moveDescriptor).toEqual(1)
  expect(question.find('QuizButton').get(1).props.moveDescriptor).toEqual(-1)
})

it('should set parity to ODD for 3 options', () => {
  const questionOdd = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'eine', 'einen']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(questionOdd.find('QuizButton').get(0).props.moveParity).toEqual('odd')
  expect(questionOdd.find('QuizButton').get(1).props.moveParity).toEqual('odd')
  expect(questionOdd.find('QuizButton').get(2).props.moveParity).toEqual('odd')
})

it('should set moveDescriptor to (1, 0, -1) for 3 options', () => {
  const question = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'eine', 'einen']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(question.find('QuizButton').get(0).props.moveDescriptor).toEqual(1)
  expect(question.find('QuizButton').get(1).props.moveDescriptor).toEqual(0)
  expect(question.find('QuizButton').get(2).props.moveDescriptor).toEqual(-1)
})

it('should set parity to EVEN for 4 options', () => {
  const questionEven = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'einen', 'eine', 'one']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(questionEven.find('QuizButton').get(0).props.moveParity).toEqual(
    'even')
  expect(questionEven.find('QuizButton').get(1).props.moveParity).toEqual(
    'even')
  expect(questionEven.find('QuizButton').get(2).props.moveParity).toEqual(
    'even')
  expect(questionEven.find('QuizButton').get(3).props.moveParity).toEqual(
    'even')
})

it('should set moveDescriptor to (2, 1, -1, -2) for 4 options', () => {
  const question = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'einen', 'eine', 'one']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(question.find('QuizButton').get(0).props.moveDescriptor).toEqual(2)
  expect(question.find('QuizButton').get(1).props.moveDescriptor).toEqual(1)
  expect(question.find('QuizButton').get(2).props.moveDescriptor).toEqual(-1)
  expect(question.find('QuizButton').get(3).props.moveDescriptor).toEqual(-2)
})

it('should set parity to ODD for 5 options', () => {
  const questionOdd = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'eine', 'einen', 'ei', 'one']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(questionOdd.find('QuizButton').get(0).props.moveParity).toEqual('odd')
  expect(questionOdd.find('QuizButton').get(1).props.moveParity).toEqual('odd')
  expect(questionOdd.find('QuizButton').get(2).props.moveParity).toEqual('odd')
  expect(questionOdd.find('QuizButton').get(3).props.moveParity).toEqual('odd')
  expect(questionOdd.find('QuizButton').get(4).props.moveParity).toEqual('odd')
})

it('should set moveDescriptor to (2, 1, 0, -1, -2) for 5 options', () => {
  const question = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'einen', 'eine', 'one', 'two']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  expect(question.find('QuizButton').get(0).props.moveDescriptor).toEqual(2)
  expect(question.find('QuizButton').get(1).props.moveDescriptor).toEqual(1)
  expect(question.find('QuizButton').get(2).props.moveDescriptor).toEqual(0)
  expect(question.find('QuizButton').get(3).props.moveDescriptor).toEqual(-1)
  expect(question.find('QuizButton').get(4).props.moveDescriptor).toEqual(-2)
})

it('should move up for good option', () => {
  const question = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'eine', 'einen']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  question.find('button').last().simulate('click')

  expect(question.find('QuizButton').get(2).props.doMove).toBeTruthy()
  expect(question.find('QuizButton').get(2).props.isWrong).toBeFalsy()
  expect(question.find('QuizButton').get(2).props.moveDescriptor).toEqual(-1)
  expect(question.find('QuizButton').get(2).props.moveParity).toEqual('odd')
})

it('should NOT move for wrong option', () => {
  const question = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'eine', 'einen']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  question.find('button').last().simulate('click')

  expect(question.find('QuizButton').get(0).props.doMove).toBeFalsy()
})

it('should NOT move for good middle option', () => {
  const question = mount(<Question questionId={9}
    start={'Ich möchte'} end={'Apfel essen.'}
    options={['ein', 'einen', 'eine']} correct={'einen'}
    nextPleaseCallBack={() => { console.log('resolveCallBack') }}
  />)

  question.find('button').last().simulate('click')

  expect(question.find('QuizButton').at(1).props.doMove).toBeFalsy()
})
