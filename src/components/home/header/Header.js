import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Container, Row, Col } from 'reactstrap';

import Navigation from './Navigation'

import './header.css'

import logo from '../logo.svg'

const Logo = () => <a href='/'>
  <img className='logo' src={logo} alt='Fehlerfrei Deutsch' />
</a>

class Header extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  }

  render () {
    return (
      <div className="headerWrapper">
        <Container>
          <Row>
            <Col xs="6" sm="3">
              <Logo />
            </Col>
            <Col xs="6" sm={{ size: 6, offset: 3 }}>
              <Navigation auth={this.props.auth}/>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default Header
