import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { NavLink } from 'react-router-dom'

import LoginButton from '../../../utils/auth/LoginButton'
import LogoutButton from '../../../utils/auth/LogoutButton'


class Navigation extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  }

  render () {
    return (
      <nav className="navigation">
        <ul>
          <li><NavLink activeClassName='active' to='/'>HOME</NavLink></li>
          <li><NavLink activeClassName='active' to='/profile'>PROFILE</NavLink></li>
          <li>{this.props.auth.isAuthenticated() ?
            <LogoutButton auth={this.props.auth} /> :
            <LoginButton auth={this.props.auth} />
          }</li>
        </ul>
      </nav>
    )
  }
}

export default Navigation
