import React from 'react'

import { Container, Button } from 'reactstrap'

import hero from './HeroPicture.jpeg'
import hero_small from './HeroPicture_small.jpeg'

import notebook from './notebook.png'
import notebook_small from './notebook_small.png'

import './hero.css'

const HeroImage = () => (
  <img className='heroImage' srcSet={hero + ' 800w, ' + hero_small + ' 200w'}
    sizes='25vw'
    src={hero_small} alt='Daumen hoch' />
)

const NotebookImage = () => (
  <img className='notebookImage' srcSet={notebook + ' 807w, ' + notebook_small + ' 240w'}
    sizes='30vw'
    src={notebook_small} alt='Fehlerfrei auf dem Computer' />
)

const HeroSection = () => (
  <section>
    <Container className='flexContainer heroContainer'>

      <HeroImage />

      <div className='flexItem heroTable'>
        <div className='heroGradient' />
        <div className='heroMessage flexContainer flexColumn'>
          <h1 className='flexItem heroHeading'>Fehlerfrei und fließend Deutsch</h1>
          <p className='flexItem'>
            Können das nur Muttersprachler?<br />
            Das Können Sie auch!<br />
            Wir helfen Ihnen dabei!<br />
            Lassen Sie Ihre Sprache aufblühen!
          </p>
          <Button className='cta flexItem'>JETZT STARTEN</Button>
        </div>
      </div>
    </Container>
    <div className='cremeHeroSection'>
      <Container>
        <p className='customerQuote'>
          "Angewöhnte Fehler zu verlernen ist nie einfacher gewesen. <br />
          <span className='fehlerfrei'>FEHLERFREI</span> ist einfach genial!"
        </p>
        <NotebookImage />
      </Container>
    </div>
  </section>
)

export default HeroSection
