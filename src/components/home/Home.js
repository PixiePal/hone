import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'

import Header from './header/Header'
import HeroSection from './hero/HeroSection'
import HowItWorks from './how/HowItWorks'
import Testimonials from './testimonials/Testimonials'
import Footer from './footer/Footer'

import Quiz from '../quiz/quiz'

import './home.css'

class Home extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  }

  render () {
    return (
      <Fragment>
        <Header auth={this.props.auth} />
        <HeroSection/>
        <HowItWorks />
        <Quiz/>
        <Testimonials />
        <Footer auth={this.props.auth} />
      </Fragment>
    )
  }
}

export default Home
