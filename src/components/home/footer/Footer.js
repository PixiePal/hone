import React, {Component} from 'react'
import { Container, Row, Col } from 'reactstrap'
import PropTypes from 'prop-types'

import './footer.css'

class Footer extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  }

  render () {
    return (
      <div className="footerWrapper">
        <Container>
          <Row>
            <Col xs={{ size: 2, offset: 1 }}>
              <p>Über uns</p>
            </Col>
            <Col xs={{ size: 2, offset: 6 }}>
              <p>Impressum</p>
            </Col>
          </Row>
          <Row>
            <Col xs={{ size: 4, offset: 4 }}>
              <p className="copyright">
                © 2018 Sofiplan GmbH
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    )
  }
}

export default Footer
