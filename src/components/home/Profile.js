import React, { Component } from 'react'
import PropTypes from 'prop-types'

class Profile extends Component {
  static propTypes = {
    auth: PropTypes.object.isRequired
  }

  componentWillMount () {
    this.setState({ profile: {} })
    const { userProfile, getProfile } = this.props.auth
    if (!userProfile) {
      getProfile((err, profile) => {
        console.log(err)
        this.setState({ profile })
      })
    } else {
      this.setState({ profile: userProfile })
    }
  }
  render () {
    const { profile } = this.state
    return (
      <div >
        <h1>{profile.name}</h1>
        <img src={profile.picture} alt='profile' />
        <div>
          <h3>{profile.nickname}</h3>
        </div>
        <pre>{JSON.stringify(profile, null, 2)}</pre>
      </div>
    )
  }
}

export default Profile
