import React from 'react'
import { Container, Row, Col, Button } from 'reactstrap'

import './testimonials.css'

import treeImage from './tree.jpg'
import treeImageXL from './tree-xl.jpg'
import treeImageXS from './tree-xs.jpg'

import Quote from './Quote'

const TreeImage = () => (
  <img className='treeImage'
    srcSet={treeImageXL + ' 2000w, '
    + treeImage + ' 1200w, ' + treeImageXS + ' 200w'}
    sizes='100vw'
    src={treeImage} alt='Jeder Baum ist einzigartig. Unser Weg auch.' />
)

const Testimonials = () =>
  <div className='testimonials'>
    <h1>Was Kunden über uns sagen</h1>
    <Container className='userTestimonials'>
      <Row className='userRow'>
        <Col sm='6'>
          <p><Quote />Ich mache immer weniger Fehler, dadurch
            wächst mein Sebstbewusstsein von Tag zu Tag.<Quote />
            <br />
            -- <strong>Maria</strong> (Fachverkäuferin)
          </p>
        </Col>
        <Col sm='6'>
          <p><Quote />FEHLERFREI erkennt schnell was ich schon gut kann und
            wo meine 'Sprachbaustellen' noch liegen. Das Spart mir
            Zeit und Aufwand, außerdem macht es richtig Spaß.<Quote /><br />
            -- <strong>Anton</strong> (Student)</p>
        </Col>
      </Row>
      <Row className='userRow'>
        <Col sm={{size: 6, offset: 3}}>
          <p><Quote />Am schönstern finde ich, dass ich jederzeit kurz üben kann,
            sei es im Bus, in die Kaffeepause oder eben im Wartezimmer
            beim Zahnarzt. So komme ich jeden Tag ein bisschen weiter mit
            meine Sprachkenntnisse.<Quote /><br />
            -- <strong>Michael</strong> (Ingenieur)</p>
        </Col>
      </Row>
    </Container>

    <div className='treeElement'>
      <TreeImage />
      <div className='greyOverlay'>
        <div className='treeMessage'>
          <p >Jeder Baum wächst <span>ein bisschen anders</span>. <br />
            So ist es auch mit der Weg zur <span>Korrekte</span> Sprache:
            bei jeder Person ist es ein bisschen anders.
          </p>
          <Button className='cta'>LERNE MIT SPASS</Button>
        </div>

      </div>
    </div>
  </div>

export default Testimonials
