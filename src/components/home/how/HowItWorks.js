import React, {Fragment} from 'react'
import { Container, Row, Col } from 'reactstrap'

import Fehlerfrei from '../Fehlerfrei'

import './how.css'

import desk_image from './coffee-charts-desk.jpg'
import desk_image_small from './coffee-charts-desk-small.jpg'

const DeskImage = () => (
  <img className='deskSeparatorImage' srcSet={desk_image + ' 1800w, ' + desk_image_small + ' 300w'}
    sizes='100vw'
    src={desk_image} alt='Der Lernfortschritt wird mit verschiedene Diagramme dargestellt' />
)

const HowItWorks = () =>
  <Fragment>
    <section className='howSection'>
      <h1>Wie funktioniert <span className='fehlerfrei'><Fehlerfrei /></span>?</h1>
      <Container>
        <Row>
          <Col sm='5' className='howSnippet'>
            <h3>Kleine 5 Minütige Übungen</h3>
            <p>mit kurze "echte" Sätze, heufig benutzte Redewendungen.</p>
          </Col>
          <Col sm={{ size: 5, offset: 2 }} className='howSnippet'>
            <h3>Kein 'starres' Lernplan</h3>
            <p>Alle Übungen werden individuell und dynamisch
            zusammengestellt, je nach Wissensstand und Lernfortschritt.</p>
          </Col>
        </Row>
        <Row>
          <Col sm={{ size: 6, offset: 3 }} className='howSnippet'>
            <h3>Sichtbares Lernerfolg</h3>
            <p>Anschauliches Übersicht der beherrschten Themen und
            der noch zu bearbeitende Aufgaben</p>
          </Col>
        </Row>
      </Container>
    </section>
    <div className='howSeparator' />
    <DeskImage />
  </Fragment>

export default HowItWorks
